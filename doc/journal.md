# Journal de recherche

# Dimanche 29 février
Publication trouvée sur une formalisation en Coq d'un générateur de parsers LL(1). Autrement dit un programme qui produit à partir d'une grammaire un parser qui reconnait cette grammaire.
Définitions de First, Follow, Nullable et critère pour être LL(1).

# Mardi 17 mars
Algo LL(1) avec arbre de dérivation en retour.