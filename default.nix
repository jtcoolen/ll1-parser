{ pkgs ? (import <nixpkgs> {}) }:
with pkgs;
stdenv.mkDerivation {
  name = "ll1parser";
  buildInputs = [
    coq_8_11
    #Report
    #texlive.combined.scheme-full
    pygmentex
    python37Packages.pygments
  ];
}
