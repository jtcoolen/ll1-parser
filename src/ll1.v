(*Inductive terminal (X : Type) : Type :=
|Terminal (x : X).

Inductive nonterminal (Y : Type) : Type :=
|NonTerminal (y : Y).

Inductive symbols (X : Type) (Y : Type): Type :=
|STerminal (x : terminal X)
|SNonTerminal (y : nonterminal Y).

Inductive list (T : Type) : Type :=
|Nil
|List (t : T) (q : list T).

Inductive option ( A : Type) : Type :=
|None
|Some ( a : A).

Inductive axiom : Type :=
|Axiome.

Inductive rule (X : Type) (Y:Type) : Type :=
|Rule (A : nonterminal Y) (u : list (symbols X Y) ).

Inductive grammar ( X : Type ) (Y : Type) : Type :=
|Gram (N : list (nonterminal Y)) (E : list (terminal X)) (S : axiom ) (P : list (rule X Y)).

Inductive prediction (X: Type) (Y:Type): Type :=
  |Prediction ( n : nonterminal Y ) ( t : terminal X ) (r : option (rule X Y )).
*)


Record grammar := {
  terminal : Type;
  nonterminal : Type;
  symbol := (terminal + nonterminal)%type;
  rule := (nonterminal * list symbol)%type;
  rules : list rule;
  axiom : nonterminal;
}.

(* 

  Example grammar:
  A -> B
  B -> b

*)

Inductive nt := A | B.
Inductive t := b.

Require Import List.
Import ListNotations.

Definition G : grammar := {|
terminal := t;
nonterminal := nt;
rules := [ (A, [inr B]); (B, [inl b]) ];
axiom := A
|}.

Print G.


Definition word alpha := list alpha.


Inductive derive (G : grammar) (w : word (terminal G)).