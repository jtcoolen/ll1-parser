Require Import List.
Require Import Notations.
Require Import Logic.
Require Import Recdef.
Import ListNotations.


(* Grammar *)
Record grammar := {
  terminal : Type;
  nonterminal : Type;
  symbol := (terminal + nonterminal)%type;
  symbols : list symbol;
  rule := (nonterminal * list symbol)%type;
  rules : list rule;
  axiom : nonterminal;
  tbeq : (terminal -> terminal -> bool);
  prooftbeq :  forall t t' : terminal, tbeq t t' = true <-> t=t';
  ntbeq : (nonterminal -> nonterminal -> bool);
  proofntbeq : forall N N' : nonterminal, ntbeq N N' = true <-> N=N';
    (*ntbeq : (preuve égalité + preuve inégalité)*)
}.

(*Definition nonterminalbeq (G : grammar) ( N : nonterminal G)  ( M :nonterminal G) : bool :=
Admitted.*)


(* Sentential forms and words *)
Definition word (G : grammar) := list (terminal G).
Definition sentence (G : grammar) := list (symbol G).

Fixpoint first_nonterminal_of_sentence_pos {G : grammar} (s : sentence G) (pos : nat) :=
  match s with
  | [] => None
  | x :: xs => 
    match x with
    | inr _ => Some pos
    | _ => first_nonterminal_of_sentence_pos xs (pos + 1)
    end
  end.

Fixpoint first_nonterminal_of_sentence {G : grammar} (s : sentence G) :=
  match s with
  | [] => None
  | x :: xs => 
    match x with
    | inr N => Some N
    | _ => first_nonterminal_of_sentence xs
    end
  end.

Definition split_sentence {G : grammar} (pos : nat) (s : sentence G) : option (sentence G * nonterminal G * sentence G) :=
  match (nth_error s pos) with
  | None => None
  | Some(x) => match x with
   | inr a => Some ((firstn (pos) s : sentence G),
                    (a : nonterminal G ),
                    (skipn (pos + 1) s : sentence G))
   | inl b => None
   end
  end.

Definition sentence_of_word {G : grammar} (w : word G) : sentence G :=
  List.map inl w.

Definition sentence_of_nonterminal {G : grammar} (nt : nonterminal G) : sentence G :=
  [ inr nt ].

Definition hd' {G : grammar} (l:list (terminal G)) :=
match l with
  | [] => None
  | x :: _ => Some x
end.

(* Definition of w:1 where w:k = wϵ if |w| < k or α if w = αγ and |α| = k. *)
Definition first_terminal_of_words_agree {G : grammar} (v : word G) (v' : word G) :=
  match hd' v, hd' v' with
  | None, None => True (* We let the empty list be the empty word by convention.
                          This corresponds to the case, v = v' = epsilon. *)
  | Some fv, Some fv' => tbeq G fv fv' = true
  | _, _ => False
  end.


(* Rules *)
Definition nonterminal_of_rule {G : grammar} (r : rule G) : nonterminal G :=
  fst r.

Definition rhs_of_rule {G : grammar} (r: rule G): sentence G :=
  snd r.

Definition apply_rule {G : grammar} (pos : nat) (r : rule G) (s : sentence G) : option (sentence G) :=
  match split_sentence pos s with
    | None => None
    | Some (prefix, nt, suffix) =>
      if (ntbeq G nt (nonterminal_of_rule r)) then
        Some (prefix ++ (rhs_of_rule r) ++ suffix)
      else
        None
  end.


(* Derivation *)
Inductive derivation (G : grammar) : Type :=
| EndOfDerivation
| ApplyRule : derivation G -> nat -> rule G -> derivation G.

Inductive derives (G : grammar) : (sentence G) -> (derivation G) -> (sentence G) -> Prop :=
| EmptyDerivation:
  forall s : sentence G,
  derives G s (EndOfDerivation G) s
| ApplyRuleDerivation:
  forall (s1 s2 s3 : sentence G) (d : derivation G) (pos : nat) (r : rule G),
  derives G s1 d s2 ->
  apply_rule pos r s2 = Some s3 ->
  derives G s1 (ApplyRule G d pos r) s3.

Definition recognize (G : grammar) (w : word G) :=
  exists d,
  derives G (sentence_of_nonterminal (axiom G)) d (sentence_of_word w).

Fixpoint apply_derivation {G : grammar} (s : sentence G) (d : derivation G) : option (sentence G) :=
  match d with
  | EndOfDerivation _ => Some s
  | ApplyRule _ d' pos r =>
    match apply_derivation s d' with
    | None => None
    | Some s' => apply_rule pos r s'
    end
  end.


(* Prove apply_derivation correct *)
Lemma apply_derivation_correct (G : grammar):
  forall s1 d s2,
  apply_derivation s1 d = Some s2 -> derives G s1 d s2.
Proof.
  induction d.
  - simpl.
    intros.
    inversion H.
    subst.
    apply EmptyDerivation.
  - simpl.
    destruct (apply_derivation s1 d).
    * intros.
      assert (H' : derives G s1 d s).
      apply IHd.
      reflexivity.
      apply ApplyRuleDerivation with (s2 := s) ; assumption.
    * congruence.
Qed.

Lemma apply_derivation_correct' (G : grammar):
  forall s1 d s2,
  derives G s1 d s2 -> apply_derivation s1 d = Some s2.
Proof.
  intros.
  induction H.
  * simpl.
    auto.
  * simpl.
    rewrite IHderives.
    assumption.
Qed.


(* Left-most derivation *)
Inductive left_derivation (G : grammar) : Type :=
| EndOfLeftDerivation
| ApplyRuleOnLeftNonTerminal : left_derivation G -> (rule G) -> left_derivation G.

Inductive derives_leftmost (G : grammar) : (sentence G) -> (left_derivation G) -> (sentence G) -> Prop :=
| EmptyLeftDerivation:
  forall s : sentence G,
  derives_leftmost G s (EndOfLeftDerivation G) s
| ApplyRuleLeftDerivation:
  forall (s1 s2 s3 : sentence G) (d : left_derivation G) (r : rule G) (pos : nat),
  derives_leftmost G s1 d s2 ->
  (first_nonterminal_of_sentence_pos s2 0) = Some pos ->
  apply_rule pos r s2 = Some s3 ->
  derives_leftmost G s1 (ApplyRuleOnLeftNonTerminal G d r) s3.

Definition derives_one_leftmost_derivation (G : grammar) (s1 : sentence G) (d : left_derivation G) (s2 : sentence G) :=
  forall r pos,
  derives_leftmost G s1 d s2 
  /\ (first_nonterminal_of_sentence_pos s1 0) = Some pos 
  /\ apply_rule pos r s1 = Some s2.

(* Left-recursive grammar *)
(* A grammar is left-recursive if and only if there exists
  a nonterminal symbol A that can derive
  to a sentential form with itself as the leftmost symbol. *)
Definition left_recursive (G : grammar) :=
  exists (A : nonterminal G) d (alpha : sentence G),
  let A' := sentence_of_nonterminal A in
  derives G A' d (A' ++ alpha).


(* LL(1) grammar *)
Definition LL1 (G : grammar) :=
  let A := sentence_of_nonterminal (axiom G) in
  forall
  d0 d1 d1' d2 d2'
  alpha alpha' gamma
  (N : nonterminal G)
  (u : word G) (v : word G) (v' : word G),
  let su := sentence_of_word u in
  let sv := sentence_of_word v in
  let sv' := sentence_of_word v' in
  let intermediate_sentence := (su ++ [inr N] ++ gamma) in

  ((derives_leftmost G A d0 intermediate_sentence
  (* The following derives is the application of the rule N -> alpha: *)
  /\ derives_one_leftmost_derivation G intermediate_sentence d1 (su ++ alpha ++ gamma)
  /\ derives G (su ++ alpha ++ gamma) d2 (su ++ sv))

  /\ (derives_leftmost G A d0 intermediate_sentence
  (* The following derives is the application of the rule N -> alpha': *)
  /\ derives_one_leftmost_derivation G intermediate_sentence d1' (su ++ alpha' ++ gamma)
  /\ derives G (su ++ alpha' ++ gamma) d2' (su ++ sv'))

  /\ first_terminal_of_words_agree v v') -> alpha = alpha'.


(*Lemma LL1_not_left_recursive (G : grammar):
  LL1 G -> not (left_recursive G).*)
 Lemma LL1_not_left_recursive (G : grammar):
  left_recursive G -> not (LL1 G).
Proof.
  unfold left_recursive.
  intros.
  unfold LL1.
  unfold derives_one_leftmost_derivation.
  admit.
  Admitted.
  (* Mq N -> alpha via apply_rule *)
  (* appliquer H à N, d1 et alpha=A alpha', mq ça boucle et qu'on a pas LL1 G. *)

(*donne une majoration*)
Fixpoint count_nonterminal (G : grammar) (list_list_symbol : list (rule G)) (n: nat) :=
  match list_list_symbol with
  |[]=>n
  |t::q => let checknont n x := match x with
                                |inl a => n
                                |inr b => S n
                                end
           in count_nonterminal G q (n+(S (fold_left checknont (snd t) 0))) 
  end.

Definition allthenonterminal (G:grammar) : nat := count_nonterminal G (rules G) 0.

(*donne une majoration*)
Fixpoint add_nonterminal (G : grammar) (list_list_symbol : list (rule G)) (l: list (nonterminal G) ) :=
  match list_list_symbol with
  |[]=>l
  |t::q => let checknont l x := match x with
                                |inl a => l
                                |inr b => b::l
                                end
           in add_nonterminal G q (l ++ (fold_left checknont (snd t) []) ) 
  end.

Definition allthenonterminal_list (G:grammar) : list (nonterminal G) := add_nonterminal G (rules G) [].

Lemma already_used_included_allthenonterminal (G : grammar) : forall N : nonterminal G, existsb (ntbeq G N) (allthenonterminal_list G) = true.
Admitted.

Definition fsize(G: grammar) (w: word G) (already_used_nonterminal_list : list (nonterminal G)): nat
  := ((length w)*(allthenonterminal G) + (allthenonterminal G  -length already_used_nonterminal_list)).

Require Import Program.
Require Import PeanoNat.

Fixpoint first_rule_of_derivation (G :grammar) (de: derivation G) : option (rule G):=
  match de with
  |EndOfDerivation _ => None
  |ApplyRule _ d n rule => match d with
                                       |EndOfDerivation _ => Some(rule)
                                       |_ => first_rule_of_derivation G d
                                       end
  end.

Definition NULLABLE {G : grammar} (alpha : sentence G) : bool.
Admitted.

Lemma NULLABLE_correct : forall G alpha, NULLABLE alpha = true <-> exists (d : derivation G), derives G alpha d [].
Proof. admit. Admitted.

Definition FIRST {G : grammar} (alpha : sentence G) : list (terminal G).
Admitted.

Definition FOLLOW {G : grammar} (alpha : sentence G) : list (terminal G).
Admitted.


Definition table_entry (G : grammar) :=
  ((nonterminal G) * (terminal G)).

Definition parse_table (G : grammar) :=
list ((table_entry G) * (rule G)).

Fixpoint prediction_fromparsetable (G : grammar) (p : parse_table G) (N : nonterminal G) (a : terminal G) : option (rule G) :=
  match p with
  |[] => None
  |t::q => let table_entry := fst t in
           let rule := snd t in
           match table_entry with
           |pair M b =>  if andb (ntbeq G M N ) (tbeq G b a) then Some(rule)
                    else prediction_fromparsetable G q N a
           end
  end. (*List. find*)

Definition first_entries {G : grammar} (r : rule G) :=
  let N := fst r in
  let alpha := snd r in
  map (fun a => ((N, a), r)) (FIRST alpha).

Definition follow_entries {G : grammar} (r : rule G) :=
  let N := fst r in
  let alpha := snd r in
  if (NULLABLE alpha) then map (fun a => ((N, a), r)) (FOLLOW alpha)
  else [].

Definition makeParseTable {G : grammar} : (parse_table G) := (*changer pour itérer sur les règles et pas symbols*)
  fold_left
  (fun p r =>
  first_entries r ++ follow_entries r ++ p)
  (rules G) [].


Lemma predict_pt_correct : forall (G : grammar) (N: nonterminal G) (a : terminal G),
                                  let pt := makeParseTable in
                                  exists (rule : rule G),
                                    prediction_fromparsetable G pt N a = Some(rule) <->
                                    (forall (d : derivation G) (s : sentence G), derives G [inr N] d ((inl a)::s)
                                                                                 -> first_rule_of_derivation G d = Some(rule)).
Proof.
Admitted.



Definition first_match_terminal (G :grammar) (s :sentence G) (a : terminal G) : Prop :=
  match s with
  |[]=> False
  |f::q => match f with
           |inl b => b = a
           |inr B => False
           end
  end.


Definition predict_ok (G : grammar) (predict : nonterminal G -> terminal G -> option (rule G)) : Prop :=

  (
    forall (N : nonterminal G) (a : terminal G),
      predict  N a = None <-> (forall (d : derivation G) (s : sentence G), derives G [inr N] d s
                                                                           -> ~ (first_match_terminal G s a))
                                )
  /\
  (
    forall (N: nonterminal G) (a : terminal G),
    exists (rule : rule G),
      predict N a = Some(rule) <->
      (forall (d : derivation G) (s : sentence G), derives G [inr N] d ((inl a)::s)
                                                   -> first_rule_of_derivation G d = Some(rule))
  ).


(* LL(1) parser *)
Program Fixpoint LL1_parser {G : grammar}
  (predict : nonterminal G -> terminal G -> option (rule G))
  (s : sentence G) (w : word G) (already_used_nonterminal_list : list (nonterminal G))
  {measure (fsize G w already_used_nonterminal_list)} : bool
  :=
    match ((allthenonterminal G) <=? (length already_used_nonterminal_list)  ) with
    |true => false
    |false =>
    match s, w with
  | [], [] => true (* case empty word *)
  | [], _ => false
  | _, [] => false
  | s :: s', a :: w' =>
    match s, a with
    | inl _, _ => LL1_parser predict s' w' [] (* accept *)(*reinitialising the list of used nonterminal*)
    | inr N, _ =>
      match predict N a with
      | None => false
      | Some (N, a'') =>
        let f x y := ntbeq G x y in
        if ( (existsb (f N) already_used_nonterminal_list )) then false (*the grammar is left_recursive*)
                         else LL1_parser predict (a'' ++ s') w (N::already_used_nonterminal_list) (* predict *)
      (* w isn't decreasing, we might either add a 'fuel' parameter
      or prove termination (grammar is not left-recursive or finite length of the leftmost derivation for w?) *)
      end
    end
    end
    end.
(*Lemma fsize_bylength_ofword : forall G w l a, (fsize G w l < fsize G (a::w) l).
Proof.
  intros.
  auto.
  Qed.*)
(*Lemma : montrer que la sentence ne contient que des nonterminal des règles *)

Require Import Omega.

Next Obligation.
  unfold fsize.
  simpl.
  apply sym_eq in Heq_anonymous.
  rewrite Nat.leb_gt in Heq_anonymous.
  omega.
Qed.
Next Obligation.
  unfold fsize.
  simpl.
  apply sym_eq in Heq_anonymous0.
  rewrite Nat.leb_gt in Heq_anonymous0.
  omega.
Qed.
Next Obligation.
  split.
  - intro.
  intro.
  intuition.
  congruence.
  -intro.
  intuition.
  congruence.
Qed.


Lemma sentence_of_word_concat_nil : forall G (a : word G), sentence_of_word (a ++ []) = sentence_of_word a ++ [].
Proof.
intros.
unfold sentence_of_word.
repeat (rewrite -> app_nil_r).
reflexivity.
Qed.


Program Fixpoint LL1_parser_deriv {G : grammar} (*rajouter le mot en entier*)
  (predict : nonterminal G -> terminal G -> option (rule G))
  (s : sentence G) (w : word G) (already_used_nonterminal_list : list (nonterminal G))
  (deriv : derivation G) (accepted : list (terminal G))
  (Hderiv:  derives G (sentence_of_nonterminal (axiom G)) deriv (sentence_of_word accepted ++ s))
  (Hpredict: predict_ok G predict)
  {measure (fsize G w already_used_nonterminal_list)} : 
  { answer : option (derivation G) |
    forall d', answer = Some d' ->
    derives G (sentence_of_nonterminal (axiom G)) d' (sentence_of_word (accepted ++ w))
  }
  :=
    let pos := length accepted in
    match ((allthenonterminal G) <=? (length already_used_nonterminal_list)  ) with
    |true => None
    |false =>
    match s, w with
  | [], [] => Some(deriv) (* case empty word *)
  | [], _ => None
  | _, [] => None
  | s :: s', a :: w' =>
    match s, a with
    | inl b, _ =>
      match (tbeq G b a) with
      |true => LL1_parser_deriv predict s' w' [] deriv (accepted ++ [a]) _ _ (* accept *)(*reinitialising the list of used nonterminal*)
      |false => None (*problem*)
      end
    | inr N, _ =>
      match predict N a with
      | None => None
      | Some (A, a'') => match (ntbeq G A N) with
                           |true =>
        let f x y := ntbeq G x y in
        if ( (existsb (f N) already_used_nonterminal_list )) then None (*the grammar is left_recursive*)
        else let deriv2 := ApplyRule _ deriv  pos (N, a'') in 
             LL1_parser_deriv predict (a'' ++ s') w (N::already_used_nonterminal_list) (deriv2) accepted _ _(* predict *)
                           |false=>None (*erreur de predict*)
                         end
      (* w isn't decreasing, we might either add a 'fuel' parameter
      or prove termination (grammar is not left-recursive or finite length of the leftmost derivation for w?) *)
      end
    end
    end
    end.

Next Obligation.
  rewrite sentence_of_word_concat_nil.
  assumption.
 Qed.

Next Obligation.
  unfold sentence_of_word.
  rewrite map_app.
  rewrite map_cons.
  simpl.
  rewrite <- app_assoc.
  simpl.
  assert (b = a).
  -rewrite <- prooftbeq.
   apply sym_eq in Heq_anonymous.
   assumption.
  -rewrite <- H.
   assumption.
Qed.
  

Next Obligation.
  unfold fsize.
  simpl.
  apply sym_eq in Heq_anonymous0.
  rewrite Nat.leb_gt in Heq_anonymous0.
  omega.
Qed.

Next Obligation.
  assert ({answer : option (derivation G) |
  forall d' : derivation G,
  answer = Some d' ->
  derives G (sentence_of_nonterminal (axiom G)) d'
          (sentence_of_word ((accepted ++ [b]) ++ w'))}).
  - apply LL1_parser_deriv with (G0 := G) (predict :=predict) (s:= s') (w := w') (deriv := deriv) (accepted := accepted++ [b])(already_used_nonterminal_list0 := already_used_nonterminal_list).
    unfold sentence_of_word.
    rewrite map_app.
    rewrite map_cons.
    simpl.
    rewrite <- app_assoc.
    simpl.
    assumption.
    assumption.
    unfold fsize.
    simpl.
    pose (Heq_anonymous1 := Heq_anonymous0).
  apply sym_eq in Heq_anonymous1.
  rewrite Nat.leb_gt in Heq_anonymous1.
  omega.
  -intros.
   simpl.
    (*eapply proj2_sig  with ( A := option (derivation G)) ( e:= d') in X.*)
  (*-eapply X .*)
Admitted.
    
Next Obligation.
  unfold predict_ok in Hpredict.
  Check apply_rule.
  assert ( apply_rule (length accepted) ( (N,a'')) (sentence_of_word accepted ++ inr N ::s') = Some (sentence_of_word accepted ++ a'' ++ s')).
  -admit.
  -apply ApplyRuleDerivation with (s2 :=  (sentence_of_word accepted ++ inr N ::s')).
   assumption.
  Admitted.

Next Obligation.
  unfold fsize.
  simpl.
  apply sym_eq in Heq_anonymous0.
  rewrite Nat.leb_gt in Heq_anonymous0.
  omega.
Qed.
Next Obligation.
  split.
  - intro.
  intro.
  intuition.
  congruence.
  -intro.
  intuition.
  congruence.
Qed.




(* Derivation tree *)
Inductive derivation_tree (G : grammar) : Type :=
| EmptyDerivationTree
| DerivationTreeNode (s : symbol G) (t : list (derivation_tree G)).


(*essai, ne construit pas bien l'arbre - peut etre vaut-il mieux partir de la racine
Fixpoint tree_of_derivation (G : grammar) (d: derivation G) (tree : derivation_tree G): derivation_tree G :=
  match d with
  |EndOfDerivation _ => tree
  |ApplyRule _ deriv nat rule  => let N := nonterminal_of_rule rule in
                                  let s := rhs_of_rule rule in
                                  match tree with
                                  |EmptyDerivationTree _ => let f x := DerivationTreeNode _ (x) ([EmptyDerivationTree _ ]) in
                                   DerivationTreeNode _ (inr N) (List.map f s)
                                  |DerivationTreeNode _ sy t => tree_of_derivation _ deriv (DerivationTreeNode _ (inr N) ([tree]))
                                  end
  end.*)

(*essai, ne fait pas bien le parcours préfixe
Fixpoint leftderivation_of_tree (G : grammar) (tree : derivation_tree G) (d : derivation G) (n :nat) : derivation G :=
  match tree with
  |EmptyDerivationTree _ => d
  |DerivationTreeNode _ s trees => let g x := match x with
                                              |EmptyDerivationTree _ => false
                                              |DerivationTreeNode _ _ _ => true
                                              end
                                   in
                                                
                                   let f x := match x with
                                              |EmptyDerivationTree _ => inr (axiom G) (*n'arrive jamais*)
                                              |DerivationTreeNode _ a trees => a
                                              end
                                   in
                                   let deriv := match s with
                                                |inl a => EndOfDerivation _
                                                |inr a => 
                                                 ApplyRule _ d n ( (pair a (List.map f (List.filter g trees))) : rule G)
                                                end in
                                   match trees with
                                   |[]=> deriv
                                   |t::q => leftderivation_of_tree _ (t) deriv n
                                   end
  end.*)



(* Compute parsing table *)

(*Axiom NULL : forall (G : grammar) (alpha:symbol G), bool.

Axiom NULL_correct : forall G alpha, NULL G alpha = true <-> exists (d : derivation G), derives G [alpha] d [].

Axiom FIRST : forall (G : grammar) (alpha : symbol G), option (terminal G).

Axiom FIRST_correct : forall G alpha a, FIRST G alpha = Some(a) <-> exists (d : derivation G) (s : sentence G), derives G [alpha] d ((inl a)::s).

Axiom FOLLOW : forall (G : grammar) (alpha : symbol G), option (terminal G).*)
(*Axiom FOLLOW_correct.*)

(*follow et first*)

(*  match alpha with
  |inl _ => false
  |inr a => 
   match rules with
   |[] => false
   |t::q => if (ntbeq G (fst t) a) then (*fonction auxilliaire à faire*)
              let checknont n x := match x with
                                   |inr a => andb n false
                                   |inl b => andb n true
                                   end
              in
              if (fold_left checknont (snd t) true) then
                let f a b := andb a (NULL G b rules) in
                fold_left f (snd t) true
              else NULL G alpha q
            else NULL G alpha q
   end
  end.*)


(*Fixpoint FIRST (G : grammar) (alpha:symbol G) (rules : list (rule G)): bool :=
  match alpha with
  |inl a => Some(a)
  |inr B =>
   match rules with
   |[]=> None
   |t::q => if ntbeq (fst t) B then
              let A::B::q := snd t in
              match A,B with
              |inl a, _ => Some(a)
              |inr A, inl b =>
               if NULL G (inr A) (rules G) then Some(b)
               else FIRST G (inr A) rules
              |inr A, inr B => if NULL G (inr A) (rules G) then FIRST G (inr B) rules
                               else FIRST G (inr A) rules
              end
            else FIRST G (inr A) q
   end
  end.*)


(*Definition rules_applying_to {G : grammar} (X : nonterminal G) : list (rule G) :=
  let f rule := ntbeq G X (fst rule) in
  filter f (rules G).

Definition prediction (G : grammar) (N : nonterminal G) (a: terminal G)
  : option (rule G) :=
  let f (rule: rule G) := match (snd rule) with
                |[] => None
                |t::q => match t with
                         |(inl a') => if tbeq G a a' then Some(rule)
                                      else None
                         |(inr A) =>
                          if NULL G (inr A) then
                            match (FOLLOW G (inr A)) with
                            |Some(a') => if tbeq G a a' then Some(rule)
                                         else None
                            |None => None (*?*)
                            end
                          else match (FIRST G (inr A)) with
                               |Some(a') => if tbeq G a a' then Some(rule)
                                            else None
                               |None => None
                               end
                         end
                end
  in let g x rule := match x with
                     |Some(_)=> x
                     |None => f rule
                     end
     in
     fold_left g (rules_applying_to N) None
.*)


(* true iff alpha ->* epsilon *)


Lemma predict_pt_correct_bis : forall (G: grammar) (N : nonterminal G) (a : terminal G),
    let pt := makeParseTable in
    prediction_fromparsetable G pt N a = None <->
    (forall (d : derivation G) (s : sentence G), derives G [inr N] d s
                                                 -> ~ (first_match_terminal G s a)).
Proof.
Admitted.

  

Lemma LL1_correct (G : grammar):
  let A := sentence_of_nonterminal (axiom G) in
  let pt := makeParseTable  in
  let predict := prediction_fromparsetable G pt in
  forall (w : word G),
  LL1 G -> recognize G w -> LL1_parser predict A w []= true.
Proof.
  intros.
  unfold recognize in H0.
  Admitted.
  


Lemma LL1_ok :
  forall (G : grammar) (predict : nonterminal G -> terminal G -> option (rule G))
    (s : sentence G) (w : word G)
    (lnont : list (nonterminal G)) (d : derivation G) ( accepted : word G)
    (Hderiv: derives G (sentence_of_nonterminal (axiom G)) d (sentence_of_word accepted ++ s))
    (Hpredict : predict_ok G predict)
    (d' : derivation G),
      proj1_sig (LL1_parser_deriv predict s w lnont d accepted Hderiv Hpredict) = Some d' ->
      derives G (sentence_of_nonterminal (axiom G)) d' (sentence_of_word (accepted ++ w)).
Proof.
  intros.
  destruct (LL1_parser_deriv predict s w lnont d accepted Hderiv Hpredict). simpl in * |-.
  subst. eapply d0; eauto.
Qed.

(*Lemma ll1_ok:
forall G predict s w lnont d accepted,
derive (axiom G) d (accepted ++ s) ->
predict_ok predict G ->
forall d', ll1_parser predict s w lnont d accepted = Some d' ->
derive (axiom G) d' (accepted ++ w).

Theorem ll1_correct:
ll1_parser predict [axiom G] w [] EndOfDerivation [] = Some d ->
derive (axiom G) d' w.
Proof.
apply ll1_ok*)

Axiom prediction_from_parsetable_ok : forall (G:grammar), predict_ok G (prediction_fromparsetable G makeParseTable).

Check proj1_sig.
Check proj2_sig.

Lemma LL1_correct'_d (G : grammar): (*correction*)
  let A := sentence_of_nonterminal (axiom G) in
  let pt := makeParseTable  in              
  let predict := prediction_fromparsetable G pt in
  forall (w : word G) (d :derivation G) ,
    predict_ok G predict ->
    (LL1_parser_deriv predict A w [] (EndOfDerivation _) []) = Some(d) ->  recognize G w.
Proof.
  intros.
  unfold recognize.
  exists d.
  eapply LL1_ok with ( G:= G) (d':=d)(accepted := []).
  -simpl.
   eapply EmptyDerivation .
  -eapply prediction_from_parsetable_ok.
  -eapply H0.
Qed.
